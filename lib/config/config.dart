import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class Config {
  final String appName = 'Tourist Guide';
  final String mapAPIKey = 'YOUR_GOOGLE_MAP_API_KEY';
  final String countryName = 'Pakistan';
  final String supportEmail = 'Jamalahmd820@gmail.com';
  final String privacyPolicyUrl = '';
  final String iOSAppId = '000000';

  final String yourWebsiteUrl = '';
  final String facebookPageUrl = '';
  final String youtubeChannelUrl = '';

  //Splash Icon
  final String splashIcon = 'assets/images/splash.png';
  final String splashIcon2 = 'assets/images/icons8.png';

  // app theme color - primary color
  static final Color appThemeColor = Colors.blueAccent;

  //special two states name that has been already upload from the admin panel
  final String specialState1 = 'Punjab';
  final String specialState2 = 'Gilgit Baltistan';

  //relplace by your country lattitude & longitude
  final CameraPosition initialCameraPosition = CameraPosition(
    target: LatLng(23.777176, 90.399452), //here
    zoom: 10,
  );

  //google maps marker icons
  final String hotelIcon = 'assets/images/hotel.png';
  final String restaurantIcon = 'assets/images/restaurant.png';
  final String hotelPinIcon = 'assets/images/hotel_pin.png';
  final String restaurantPinIcon = 'assets/images/restaurant_pin.png';
  final String drivingMarkerIcon = 'assets/images/driving_pin.png';
  final String destinationMarkerIcon =
      'assets/images/destination_map_marker.png';

  //Intro images
  final String introImage1 = 'assets/images/travel6.png';
  final String introImage2 = 'assets/images/travel1.png';
  final String introImage3 = 'assets/images/travel5.png';

  //Language Setup
  final List<String> languages = ['English', 'Spanish', 'Arabic'];
}

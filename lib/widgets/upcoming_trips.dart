import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:travel_hour/blocs/advertise_trip_bloc.dart';
import 'package:travel_hour/models/trip.dart';
import 'package:travel_hour/pages/advertised_trips.dart';
import 'package:travel_hour/pages/trip_details.dart';
import 'package:travel_hour/utils/loading_cards.dart';
import 'package:travel_hour/utils/next_screen.dart';
import 'package:travel_hour/widgets/custom_cache_image.dart';

class UpComingTrips extends StatelessWidget {
  const UpComingTrips({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ub = context.watch<AdvertiseTripBloc>();

    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(left: 15, top: 15, right: 10),
          child: Row(children: <Widget>[
            Text('Upcoming Trips', style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
                color: Colors.grey[900],
                wordSpacing: 1,
                letterSpacing: -0.6
            ),
            ),
            Spacer(),
            IconButton(icon: Icon(Icons.arrow_forward),
              onPressed: () => nextScreen(context, AdvertisedTripsList(isFromUser: true)),
            )
          ],),
        ),

        Container(
          height: 245,
          width: MediaQuery.of(context).size.width,
          child: ListView.builder(
            padding: EdgeInsets.only(left: 15, right: 15),
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            itemCount: ub.upcomingList.isEmpty ? 3 : ub.upcomingList.length,
            itemBuilder: (BuildContext context, int index) {
              if(ub.upcomingList.isEmpty) return LoadingPopularPlacesCard();
              return _ItemList(d: ub.upcomingList[index],);
              //return LoadingCard1();
            },
          ),
        )

      ],
    );
  }
}

class _ItemList extends StatelessWidget {
  final Trip d;
  const _ItemList({Key? key, required this.d}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Container(
        margin: EdgeInsets.only(left: 0, right: 10, top: 5, bottom: 5),
        width: MediaQuery.of(context).size.width * 0.40,
        decoration: BoxDecoration(
            color: Colors.grey[300],
            borderRadius: BorderRadius.circular(10)

        ),
        child: Stack(
          children: [
            Hero(
              tag: 'upcoming${d.id}',
              child: ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: CustomCacheImage(imageUrl: d.imgUrl)
              ),
            ),
            Align(
              alignment: Alignment.bottomLeft,
              child: Padding(
                padding: const EdgeInsets.only(bottom: 20, left: 10, right: 10),
                child: Text(d.name!,
                  maxLines: 2,
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                      color: Colors.white
                  ),),
              ),
            ),

          ],
        ),

      ),

      onTap: () => nextScreen(context, TripDetails(trip: d)),
    );
  }
}

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:travel_hour/blocs/sign_in_bloc.dart';
import 'package:travel_hour/models/company.dart';
import 'package:provider/provider.dart';
import 'package:travel_hour/pages/company_signin.dart';
import 'package:travel_hour/pages/done.dart';
import 'package:travel_hour/utils/next_screen.dart';

class CompanyRegistrationBloc extends ChangeNotifier {

  bool isLoading  = false;

  Company company = Company();

  GlobalKey<FormState> formKey = GlobalKey();
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey();

  BuildContext context;

  CompanyRegistrationBloc(this.context);


  Future<void> registerCompany(String? tag, BuildContext _context) async {
    FocusScope.of(_context).requestFocus(FocusNode());
    if(formKey.currentState?.validate() ?? false) {
      formKey.currentState?.save();
      isLoading = true;
      notifyListeners();
      //TODO: Add Company to Firestore
      print(this.company.toJson());
      final FirebaseAuth _firebaseAuth = this.context.read<SignInBloc>().firebaseAuth;
      try {
        await _firebaseAuth.signOut();
      } catch (e) {}
      _firebaseAuth.createUserWithEmailAndPassword(email: this.company.email!, password: this.company.password!).then((value) async {
        if(value != null && value.user != null) {
          final user = value.user!;
          final id = user.uid;
          this.company.id = id;
          Map<String, dynamic> _company = this.company.toJson();
          final FirebaseFirestore _fireStore = FirebaseFirestore.instance;
          // final FirebaseMessaging _messaging = FirebaseMessaging.instance;
          // final _token = await _messaging.getToken();
          // if(_token != null) {
          //   _company['token'] =  _token;
          // }
          _company['loved blogs'] = [];
          _company['loved places'] = [];
          _company['bookmarked blogs'] = [];
          _company['bookmarked places'] = [];
          _fireStore.collection('companies').doc(this.company.id).set(_company).then((companyValue) async {
              //Company added to Firestore
              await value.user!.sendEmailVerification();
              try {
                _firebaseAuth.signOut();
              } catch(e) {}
              ScaffoldMessenger.of(this.scaffoldKey.currentContext!).showSnackBar(SnackBar(content: Text('A verification email has been sent to your email.')));
              isLoading = false;
              // this.context.read<SignInBloc>().setIsCompany(true);
              // this.context.read<SignInBloc>().company = this.company;
              // //TODO: Navigate to HOME Screen
              // await this.context.read<SignInBloc>().setSignIn();
              // this.context.read<SignInBloc>().setUserDetails(this.company.name!, this.company.email!, this.company.imgUrl, this.company.id!);
              // this.context.read<SignInBloc>().saveDataToSP();
              notifyListeners();
              formKey.currentState?.reset();
              // afterSignIn(tag, _context);
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(builder: (context) => ChangeNotifierProvider(
                  create: (ctx) => CompanyRegistrationBloc(ctx),
                  child: CompanySignInWidget(),
                )),
              );
          }).catchError((e) async {
            //Error adding company
            ScaffoldMessenger.of(this.scaffoldKey.currentContext!).showSnackBar(SnackBar(content: Text('Error adding this company')));
            try {
              await _firebaseAuth.signOut();
            } catch (e) {

            }
            isLoading = false;
            notifyListeners();
          });
        } else {
          //Error Registering
          ScaffoldMessenger.of(this.scaffoldKey.currentContext!).showSnackBar(SnackBar(content: Text('Error adding this company')));
          try {
            await _firebaseAuth.signOut();
          } catch (e) {

          }
          isLoading = false;
          notifyListeners();
        }
      }).catchError((e) {
        //Error Registering now
        if(e?.code == 'email-already-in-use') {
          ScaffoldMessenger.of(this.scaffoldKey.currentContext!).showSnackBar(
              SnackBar(content: Text('Email already registered')));
        } else {
          ScaffoldMessenger.of(this.scaffoldKey.currentContext!).showSnackBar(
              SnackBar(content: Text('Error in Registration')));
        }
        isLoading = false;
        notifyListeners();
      });
    }
  }


  Future<void> loginCompany(String? tag, BuildContext _context) async {
    FocusScope.of(_context).requestFocus(FocusNode());
    if(formKey.currentState?.validate() ?? false) {
      formKey.currentState?.save();
      isLoading = true;
      notifyListeners();
      //Add Company to Firestore
      print(this.company.toJson());
      final FirebaseAuth _firebaseAuth = this.context.read<SignInBloc>().firebaseAuth;
      try {
        await _firebaseAuth.signOut();
      } catch (e) {}
      _firebaseAuth.signInWithEmailAndPassword(email: this.company.email!, password: this.company.password!).then((value) async {
        if(value != null && value.user != null) {
          if(value.user!.emailVerified) {
            final FirebaseFirestore _fireStore = FirebaseFirestore.instance;
            _fireStore.collection('companies').doc(value.user!.uid).get().then((value) async {
              if(value != null && value.exists && value.data() != null) {
                final data = value.data()!;
                this.company = Company.fromJson(data);

                final FirebaseMessaging _messaging = FirebaseMessaging.instance;
                final _token = await _messaging.getToken();
                if(_token != null) {
                  Map<String, dynamic> _company = this.company.toJson();
                  _company['token'] =  _token;
                  _fireStore.collection('companies').doc(this.company.id).set(_company);
                }

                print('Signed In as $data');
                isLoading = false;
                //TODO: Navigate to HOME Screen
                this.context.read<SignInBloc>().setIsCompany(true);
                this.context.read<SignInBloc>().company = this.company;
                await this.context.read<SignInBloc>().setSignIn();
                this.context.read<SignInBloc>().setUserDetails(this.company.name!, this.company.email!, this.company.imgUrl, this.company.id!);
                this.context.read<SignInBloc>().saveDataToSP();
                notifyListeners();
                formKey.currentState?.reset();
                afterSignIn(tag, _context);
              } else {
                //Error Registering
                ScaffoldMessenger.of(this.scaffoldKey.currentContext!).showSnackBar(SnackBar(content: Text('No company found with this email')));
                try {
                  await _firebaseAuth.signOut();
                } catch (e) {}
                isLoading = false;
                notifyListeners();
              }
            });
          } else {
            await value.user!.sendEmailVerification();
            try {
              _firebaseAuth.signOut();
            } catch(e) {}
            isLoading = false;
            notifyListeners();
            formKey.currentState?.reset();
            ScaffoldMessenger.of(this.scaffoldKey.currentContext!).showSnackBar(SnackBar(content: Text('Please verify your email. A verification email has been sent.')));
          }
        } else {
          //Error Registering
          ScaffoldMessenger.of(this.scaffoldKey.currentContext!).showSnackBar(SnackBar(content: Text('No user found')));
          try {
            await _firebaseAuth.signOut();
          } catch (e) {}
          isLoading = false;
          notifyListeners();
        }
      }).catchError((e) {
        ScaffoldMessenger.of(this.scaffoldKey.currentContext!).showSnackBar(SnackBar(content: Text('Error logging in')));
        isLoading = false;
        notifyListeners();
      });
    }
  }

  resetPassword(BuildContext _context) {
    FocusScope.of(_context).requestFocus(FocusNode());
    if(formKey.currentState?.validate() ?? false) {
      formKey.currentState?.save();
      isLoading = true;
      notifyListeners();
      final FirebaseAuth _firebaseAuth = this.context.read<SignInBloc>().firebaseAuth;
      _firebaseAuth.sendPasswordResetEmail(email: this.company.email!).then((value) {
        isLoading = false;
        formKey.currentState?.reset();
        notifyListeners();
        ScaffoldMessenger.of(this.scaffoldKey.currentContext!).showSnackBar(SnackBar(content: Text('Password reset Email is sent to your email')));
        Navigator.pop(_context);
      })
          .catchError((e) {
        ScaffoldMessenger.of(this.scaffoldKey.currentContext!).showSnackBar(SnackBar(content: Text('Error resetting password')));
        isLoading = false;
        notifyListeners();
      });
    }
  }

  afterSignIn(String? tag, BuildContext _context) {
    if (tag == null) {
      nextScreen(_context, DonePage());
    } else {
      Navigator.pop(_context);
    }
  }

}
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:travel_hour/blocs/sign_in_bloc.dart';
import 'package:travel_hour/models/trip.dart';

class AdvertiseTripBloc extends ChangeNotifier {

  Trip trip = Trip();
  List<Trip> trips = [];
  List<Trip> upcomingList = [];

  bool isLoading = false;

  GlobalKey<FormState> formKey = GlobalKey();
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey();

  BuildContext context;
  AdvertiseTripBloc(this.context);

  setTrip(Trip _trip){
    this.trip = _trip;
    notifyListeners();
  }

  addTripToFirestore(BuildContext _context) async {
    FirebaseFirestore _fireStore = FirebaseFirestore.instance;
    _fireStore.collection('trips').doc(this.trip.id!).set(this.trip.toJson()).then((value) {
      this.isLoading  =false;
      notifyListeners();
      ScaffoldMessenger.of(_context).showSnackBar(SnackBar(content: Text('Trip added Successfully')));
      Navigator.pop(_context);
    }).catchError((e) {
      print(e);
      this.isLoading  =false;
      notifyListeners();
      ScaffoldMessenger.of(_context).showSnackBar(SnackBar(content: Text('Unable to add Trip, Try again later please')));
    });
  }
  
  getMyAdvertisedTrips(BuildContext _ctx) async {
    isLoading = true;
    trips.clear();
    notifyListeners();
    final _uid = context.read<SignInBloc>().uid;
    FirebaseFirestore _fireStore = FirebaseFirestore.instance;
    _fireStore.collection('trips')
        .where('companyId', isEqualTo: _uid).get().then((querySnap) {
      if(querySnap.docs.isNotEmpty) {
        for(int i=0; i< querySnap.docs.length; i++) {
          final doc = querySnap.docs[i];
          if(doc.exists) {
            trips.add(Trip.fromJson(doc.data()));
          }
        }
        isLoading = false;
        notifyListeners();
      } else {
        ScaffoldMessenger.of(_ctx).showSnackBar(SnackBar(content: Text('You don\'t have any trips added')));
        isLoading = false;
        notifyListeners();
      }
    })
        .catchError((e) {
          ScaffoldMessenger.of(_ctx).showSnackBar(SnackBar(content: Text('Unable to fetch your Trips')));
      isLoading = false;
      notifyListeners();
    });
  }

  getAllAdvertisedTrips(BuildContext _ctx) async {
    isLoading = true;
    trips.clear();
    notifyListeners();
    final _uid = context.read<SignInBloc>().uid;
    FirebaseFirestore _fireStore = FirebaseFirestore.instance;
    _fireStore.collection('trips').get().then((querySnap) {
      if(querySnap.docs.isNotEmpty) {
        for(int i=0; i< querySnap.docs.length; i++) {
          final doc = querySnap.docs[i];
          if(doc.exists) {
            trips.add(Trip.fromJson(doc.data()));
          }
        }
        isLoading = false;
        notifyListeners();
      } else {
        ScaffoldMessenger.of(_ctx).showSnackBar(SnackBar(content: Text('You don\'t have any trips added')));
        isLoading = false;
        notifyListeners();
      }
    })
        .catchError((e) {
      ScaffoldMessenger.of(_ctx).showSnackBar(SnackBar(content: Text('Unable to fetch your Trips')));
      isLoading = false;
      notifyListeners();
    });
  }

  getlimitedAdvertisedTrips(BuildContext _ctx) async {
    upcomingList.clear();
    notifyListeners();
    FirebaseFirestore _fireStore = FirebaseFirestore.instance;
    _fireStore.collection('trips').limit(5).get().then((querySnap) {
      if(querySnap.docs.isNotEmpty) {
        for(int i=0; i< querySnap.docs.length; i++) {
          final doc = querySnap.docs[i];
          if(doc.exists) {
            upcomingList.add(Trip.fromJson(doc.data()));
          }
        }
        notifyListeners();
      } else {
        ScaffoldMessenger.of(_ctx).showSnackBar(SnackBar(content: Text('You don\'t have any trips added')));
      }
    })
        .catchError((e) {
      ScaffoldMessenger.of(_ctx).showSnackBar(SnackBar(content: Text('Unable to fetch your Trips')));
    });
  }

  deleteTrip(BuildContext _ctx, Trip _trip) async {
    FirebaseFirestore _fireStore = FirebaseFirestore.instance;
    _fireStore.collection('trips').doc(_trip.id!).delete().then((value) {
      trips.removeWhere((element) => element.id == _trip.id);
      notifyListeners();
      ScaffoldMessenger.of(_ctx).showSnackBar(SnackBar(content: Text('Trip deleted')));
    }).catchError((e) {
      ScaffoldMessenger.of(_ctx).showSnackBar(SnackBar(content: Text('Unable to delete Trip, Try later please')));
    });
  }

}
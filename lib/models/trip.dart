class Trip {

  String? id;
  String? companyId;
  String? name;
  String? from;
  String? to;
  int? price;
  String? imgUrl;
  String? schedule;//DateTime in format of dd/MM/yyyy hh:mm AM/PM
  int? duration;

  Trip({
    this.imgUrl, this.id, this.name, this.duration, this.from, this.schedule, this.price, this.to
  });

  Trip.fromJson(Map<String, dynamic> data) {
    this.id = data['id'];
    this.companyId = data['companyId'];
    this.name = data['name'];
    this.from = data['from'];
    this.to = data['to'];
    this.price = data['price'];
    this.imgUrl = data['imgUrl'];
    this.schedule = data['schedule'];
    this.duration = data['duration'];
  }

  Map<String, dynamic> toJson(){
    Map<String, dynamic> data = Map();
    data['id'] = this.id;
    data['companyId'] = this.companyId;
    data['name'] = this.name;
    data['from'] = this.from;
    data['to'] = this.to;
    data['price'] = this.price;
    data['imgUrl'] = this.imgUrl;
    data['schedule'] = this.schedule;
    data['duration'] = this.duration;
    return data;
  }



}
class Company {

  String? id;
  String? name;
  String? regNo;
  String? address;
  String? phone;
  String? email;
  String? password;
  int status = 0; //0 == inactive, 1 == active/accept, -1 == De-active/Rejected
  String imgUrl = '';
  String? token;

  Company({
    this.id,
    this.address,
    this.name,
    this.phone,
    this.regNo,
    this.status = 0,
    this.email,
    this.password,
    this.imgUrl = '',
    this.token,
  });

  Company.fromJson(Map<String, dynamic> data){
    id = data['id'];
    name = data['name'];
    regNo = data['regNo'];
    address = data['address'];
    phone = data['phone'];
    status = data['status'];
    email = data['email'];
    imgUrl = data['imgUrl'] ?? '';
    token = data['token'] ?? null;
  }

  Map<String, dynamic> toJson(){
    Map<String, dynamic> data = Map();
    data['id'] = this.id;
    data['name'] = this.name;
    data['regNo'] = this.regNo;
    data['address'] = this.address;
    data['phone'] = this.phone;
    data['status'] = this.status;
    data['email'] = this.email;
    data['imgUrl'] = this.imgUrl;
    data['token'] = this.token;
    return data;
  }

}
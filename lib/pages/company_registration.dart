import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:travel_hour/blocs/company_registration_bloc.dart';

import 'company_signin.dart';

class CompanyRegistration extends StatefulWidget {
  final String? tag;
  const CompanyRegistration({Key? key, this.tag}) : super(key: key);

  @override
  _CompanyRegistrationState createState() => _CompanyRegistrationState();
}

class _CompanyRegistrationState extends State<CompanyRegistration> {
  @override
  Widget build(BuildContext context) {

    final bloc = context.watch<CompanyRegistrationBloc>();

    return WillPopScope(
      onWillPop: () async {
        return !bloc.isLoading;
      },
      child: Scaffold(
        key: bloc.scaffoldKey,
        body: Stack(
          children: [
            SafeArea(
              child: SingleChildScrollView(
                child: Form(
                  key: bloc.formKey,
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10.0),
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(18.0),
                          child: Container(
                            child: Text(
                              'Register Here',
                              style: TextStyle(fontSize: 20),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(10),
                          child: TextFormField(
                            validator: (value) => value == null || value.length < 3 ? 'Must Contains at least 3 characters' : null,
                              decoration: InputDecoration(
                                  hintText: 'Company Name',
                                  contentPadding:
                                      EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: Colors.black),
                                  ),
                                  border: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.black))),
                            onSaved: (value) {
                              context.read<CompanyRegistrationBloc>().company.name = value;
                            },),
                        ),

                        Padding(
                          padding: const EdgeInsets.all(10),
                          child: TextFormField(
                            validator: (value) => value == null || !value.contains("@") || !value.contains(".") ? 'Must be a valid email' : null,
                            decoration: InputDecoration(
                                hintText: 'Email Address',
                                contentPadding:
                                EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.black),
                                ),
                                border: OutlineInputBorder(
                                    borderSide: BorderSide(color: Colors.black))),
                            onSaved: (value) {
                              context.read<CompanyRegistrationBloc>().company.email = value;
                            },),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(10),
                          child: TextFormField(
                              validator: (value) => value == null || value.length < 6 ? 'Should be 6 characters long' : null,
                              decoration: InputDecoration(
                                  hintText: 'Company Registration Number',
                                  contentPadding:
                                      EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: Colors.black),
                                  ),
                                  border: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.black))),
                            onSaved: (value) {
                              context.read<CompanyRegistrationBloc>().company.regNo = value;
                            },),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(10),
                          child: TextFormField(
                              validator: (value) => value == null || value.length < 10 ? 'Should be 10 characters long' : null,
                              decoration: InputDecoration(
                                  hintText: 'Company Address',
                                  contentPadding:
                                      EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: Colors.black),
                                  ),
                                  border: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.black))),
                            onSaved: (value) {
                              context.read<CompanyRegistrationBloc>().company.address = value;
                            },),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(10),
                          child: TextFormField(
                              validator: (value) => value == null || value.length < 1 || !value.startsWith("+") ? 'Should be a valid phone number' : null,
                              decoration: InputDecoration(
                                  hintText: 'Company Phone',
                                  contentPadding:
                                      EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: Colors.black),
                                  ),
                                  border: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.black))),
                            onSaved: (value) {
                                context.read<CompanyRegistrationBloc>().company.phone = value;
                            },
                          ),
                        ),

                        Padding(
                          padding: const EdgeInsets.all(10),
                          child: TextFormField(
                            obscureText: true,
                            validator: (value) => value == null || value.length < 6 ? 'Must be 6 characters long' : null,
                            decoration: InputDecoration(
                                hintText: 'Password',
                                contentPadding:
                                EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.black),
                                ),
                                border: OutlineInputBorder(
                                    borderSide: BorderSide(color: Colors.black))),
                            onSaved: (value) {
                              context.read<CompanyRegistrationBloc>().company.password = value;
                            },),
                        ),
                        // SizedBox(
                        //   height: 20,
                        // ),
                        Padding(
                          padding: const EdgeInsets.all(10),
                          child: MaterialButton(
                            onPressed: () {
                              context.read<CompanyRegistrationBloc>().registerCompany(widget.tag, context);
                              },
                            minWidth: double.infinity,
                            height: 50,
                            color: Colors.blue,
                            elevation: 0,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(50),
                            ),
                            child: Text(
                              'Register Now',
                              style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 18,
                                  color: Colors.white),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(10),
                          child: TextButton(
                            onPressed: (){
                              Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(builder: (context) => ChangeNotifierProvider(
                                  create: (ctx) => CompanyRegistrationBloc(ctx),
                                  child: CompanySignInWidget(tag: widget.tag,),
                                )),
                              );
                            },
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  'Already Registered? Login here',
                                  style: TextStyle(
                                      fontWeight: FontWeight.w400,
                                      fontSize: 14,
                                      color: Colors.black54),
                                ),
                              ],
                            ),
                          )
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),


            bloc.isLoading ? Container(
              height: MediaQuery.of(context).size.height,
              width:  MediaQuery.of(context).size.width,
              color: Colors.white.withOpacity(0.5),
              child: Center(child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    height: 40,
                    child: CircularProgressIndicator(),
                  )
                ],
              )),
            ) : SizedBox(),
          ],
        ),
      ),
    );
  }
}

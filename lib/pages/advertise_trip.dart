import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:travel_hour/blocs/advertise_trip_bloc.dart';
import 'package:travel_hour/blocs/sign_in_bloc.dart';
import 'package:travel_hour/models/trip.dart';

class AdvertiseTrip extends StatefulWidget {
  bool isForEditing = false;
  Trip? trip;
  AdvertiseTrip({Key? key, this.isForEditing = false, this.trip}) : super(key: key);

  @override
  _AdvertiseTripState createState() => _AdvertiseTripState();
}

class _AdvertiseTripState extends State<AdvertiseTrip> {
  File? imageFile;
  String? fileName;
  TextEditingController _datetimeController = TextEditingController();
  TextEditingController _nameController = TextEditingController();
  TextEditingController _fromController = TextEditingController();
  TextEditingController _toController = TextEditingController();
  TextEditingController _priceController = TextEditingController();
  TextEditingController _durationController = TextEditingController();

  @override
  initState(){
    _initialize();
    super.initState();
  }

  _initialize() async {
    SchedulerBinding.instance?.addPostFrameCallback((timeStamp) {
      final bloc = context.read<AdvertiseTripBloc>();
      final sb = context.read<SignInBloc>();
      bloc.trip.companyId = sb.uid!;

      if(widget.isForEditing) {
        bloc.setTrip(widget.trip!);
        print('${bloc.trip.name}');
        _datetimeController.text = bloc.trip.schedule!;
        _nameController.text = bloc.trip.name!;
        _fromController.text = bloc.trip.from!;
        _toController.text = bloc.trip.to!;
        _priceController.text = bloc.trip.price!.toString();
        _durationController.text = bloc.trip.duration!.toString();
      }
    });
  }

  Future pickImage() async {
    final  _imagePicker = ImagePicker();
    var imagepicked = await _imagePicker.pickImage(source: ImageSource.gallery, maxHeight: 200, maxWidth: 200);

    if (imagepicked != null) {
      setState(() {
        imageFile = File(imagepicked.path);
        fileName = (imageFile!.path);
      });
    } else {
      print('No image has is selected!');

    }
  }

  _displayDateAndTimePicker(){
    FocusScope.of(context).requestFocus(FocusNode());
    //DateTime in format of dd/MM/yyyy hh:mm AM/PM
    DatePicker.showDateTimePicker(context,
        showTitleActions: true,
        minTime: DateTime.now().add(Duration(days: 1)),
        maxTime: DateTime.now().add(Duration(days: 365)),
        onChanged: (date) {
          print('change $date');
        }, onConfirm: (date) {
          //DateTime in format of dd/MM/yyyy hh:mm AM/PM
          final _formatted = _changeDateTimeToDesiredFormat(date);
          // final reversedDate = _changeStringToDateTime(_formatted);
          _datetimeController.text = _formatted;
        },
        currentTime: DateTime.now(), locale: LocaleType.en);
  }

  String _changeDateTimeToDesiredFormat(DateTime date) {
    DateFormat _format = DateFormat('dd/MM/yyyy - hh:mm a');
    return _format.format(date);
  }

  DateTime _changeStringToDateTime(String dateTime) {
    DateFormat _format = DateFormat('dd/MM/yyyy - hh:mm a');
    return _format.parse(dateTime);
  }

  Future uploadImage() async {
      final bloc = context.read<AdvertiseTripBloc>();
      final _id = widget.isForEditing ? bloc.trip.id! : new DateTime.now().millisecondsSinceEpoch;
      if(!widget.isForEditing) {
        bloc.trip.id = _id.toString();
      }
      Reference storageReference = FirebaseStorage.instance.ref().child(
          'Trip Pictures/$_id');
      UploadTask uploadTask = storageReference.putFile(imageFile!);

      await uploadTask.whenComplete(() async {
        var _url = await storageReference.getDownloadURL();
        var _imageUrl = _url.toString();
        bloc.trip.imgUrl = _imageUrl;
      });
  }


  _addTripToFirestore() async {
    if(imageFile == null || fileName == null) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('You must add a trip image')));
    } else {
      final bloc = context.read<AdvertiseTripBloc>();
      if(bloc.formKey.currentState?.validate() ?? false) {

        bloc.formKey.currentState?.save();
        bloc.isLoading = true;
        bloc.notifyListeners();

        await uploadImage()
            .then((value) {
          bloc.addTripToFirestore(context);
        }).catchError((e){
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('Error Uploading Image, Try again please')));
          bloc.isLoading = false;
          bloc.notifyListeners();
        });

      }
    }
  }

  _updateTrip() async {
    final bloc = context.read<AdvertiseTripBloc>();
    if(bloc.formKey.currentState?.validate() ?? false) {

      bloc.formKey.currentState?.save();
      bloc.isLoading = true;
      bloc.notifyListeners();

      if(this.imageFile != null && this.fileName != null) {
        await uploadImage()
            .then((value) {
          bloc.addTripToFirestore(context);
        }).catchError((e){
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('Error Uploading Image, Try again please')));
          bloc.isLoading = false;
          bloc.notifyListeners();
        });
      } else {
        bloc.addTripToFirestore(context);
      }

    }
  }

  @override
  Widget build(BuildContext context) {

    final bloc = context.watch<AdvertiseTripBloc>();
    final trip = bloc.trip;
    final sp = context.watch<SignInBloc>();

    return WillPopScope(
      onWillPop: () async {
        return !bloc.isLoading;
      },
      child: Scaffold(
        key: bloc.scaffoldKey,
        body: Stack(
          children: [

            SafeArea(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 10),
                child: Form(
                  key: bloc.formKey,
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(18.0),
                          child: Container(
                            child: Text(
                              widget.isForEditing ? 'Edit Trip' : 'Add Trip',
                              style: TextStyle(fontSize: 20),
                            ),
                          ),
                        ),
                        sp.company.status == 0
                        ?
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [

                            Padding(
                              padding: const EdgeInsets.all(10),
                              child: Text(
                                'Your Company\'s profile is Under Review.\nPlease wait for the Profile\'s approval\n\nOnce your profile is approved, you\'ll bne able to advertise your trips',
                                style: TextStyle(fontSize: 15),
                                textAlign: TextAlign.center,
                              ),
                            ),

                            Padding(
                              padding: const EdgeInsets.all(10),
                              child: MaterialButton(
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                minWidth: double.infinity,
                                height: 50,
                                color: Colors.blue,
                                elevation: 0,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(50),
                                ),
                                child: Text(
                                  'Go Back',
                                  style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 18,
                                      color: Colors.white),
                                ),
                              ),
                            )
                          ],
                        )
                            : sp.company.status == -1
                        ? Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [

                            Padding(
                              padding: const EdgeInsets.all(10),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Text(
                                    'WE ARE SORRY.',
                                    style: TextStyle(fontSize: 15, color: Colors.red),
                                    textAlign: TextAlign.center,
                                  ),
                                  Text(
                                    'We didn\'t find your information correct to advertise Trips on System',
                                    style: TextStyle(fontSize: 15),
                                    textAlign: TextAlign.center,
                                  )
                                ],
                              ),
                            ),

                            Padding(
                              padding: const EdgeInsets.all(10),
                              child: MaterialButton(
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                minWidth: double.infinity,
                                height: 50,
                                color: Colors.blue,
                                elevation: 0,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(50),
                                ),
                                child: Text(
                                  'Go Back',
                                  style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 18,
                                      color: Colors.white),
                                ),
                              ),
                            )
                          ],
                        )
                        : Column(
                          children: [
                            InkWell(
                              child: CircleAvatar(
                                radius: 70,
                                backgroundColor: Colors.grey[300],
                                child: Container(
                                  height: 120,
                                  width: 120,

                                  decoration: BoxDecoration(
                                      border: Border.all(
                                          width: 1,
                                          color: Colors.grey[800]!
                                      ),

                                      color: Colors.grey[500],
                                      shape: BoxShape.circle,
                                      image: DecorationImage(
                                          image: (
                                              imageFile == null
                                              ? bloc.trip.imgUrl != null && bloc.trip.imgUrl!.length > 1
                                              ? CachedNetworkImageProvider(bloc.trip.imgUrl!)
                                                  : AssetImage('assets/images/splash_guide.png')
                                              :
                                          FileImage(imageFile!)) as ImageProvider<Object>,
                                          fit: BoxFit.cover)),
                                  child: Align(
                                      alignment: Alignment.bottomRight,
                                      child: Icon(
                                        Icons.edit,
                                        size: 30,
                                        color: Colors.black,
                                      )),
                                ),
                              ),
                              onTap: (){
                                pickImage();
                              },
                            ),

                            Padding(
                              padding: const EdgeInsets.all(10),
                              child: TextFormField(
                                controller: _nameController,
                                validator: (value) => value == null || value.length < 6 ? 'Must contains att lease 6 characters' : null,
                                decoration: InputDecoration(
                                    hintText: 'Title',
                                    contentPadding:
                                    EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                                    enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.black),
                                    ),
                                    border: OutlineInputBorder(
                                        borderSide: BorderSide(color: Colors.black))),
                                onSaved: (value) {
                                  context.read<AdvertiseTripBloc>().trip.name = value;
                                },),
                            ),

                            Padding(
                              padding: const EdgeInsets.all(10),
                              child: TextFormField(
                                controller: _fromController,
                                validator: (value) => value == null || value.length < 4 ? 'Must contains at least 4 characters' : null,
                                decoration: InputDecoration(
                                    hintText: 'Starting Location',
                                    contentPadding:
                                    EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                                    enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.black),
                                    ),
                                    border: OutlineInputBorder(
                                        borderSide: BorderSide(color: Colors.black))),
                                onSaved: (value) {
                                  context.read<AdvertiseTripBloc>().trip.from = value;
                                },),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(10),
                              child: TextFormField(
                                controller: _toController,
                                validator: (value) => value == null || value.length < 4 ? 'Must contains at least 4 characters' : null,
                                decoration: InputDecoration(
                                    hintText: 'Destination',
                                    contentPadding:
                                    EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                                    enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.black),
                                    ),
                                    border: OutlineInputBorder(
                                        borderSide: BorderSide(color: Colors.black))),
                                onSaved: (value) {
                                  context.read<AdvertiseTripBloc>().trip.to = value;
                                },),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(10),
                              child: TextFormField(
                                controller: _priceController,
                                validator: (value) => value == null || value.length < 2 ? 'Must contains at least 2 characters' : null,
                                keyboardType: TextInputType.numberWithOptions(signed: false, decimal: false),
                                inputFormatters: [
                                  FilteringTextInputFormatter.digitsOnly
                                ],
                                decoration: InputDecoration(
                                    hintText: 'Charges per head',
                                    contentPadding:
                                    EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                                    enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.black),
                                    ),
                                    border: OutlineInputBorder(
                                        borderSide: BorderSide(color: Colors.black))),
                                onSaved: (value) {
                                  context.read<AdvertiseTripBloc>().trip.price = int.parse(value!);
                                },),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(10),
                              child: TextFormField(
                                controller: _durationController,
                                validator: (value) => value == null || value.length < 1 || int.parse(value) == 0 ? 'Can\'t be empty or 0' : null,
                                keyboardType: TextInputType.numberWithOptions(signed: false, decimal: false),
                                inputFormatters: [
                                  FilteringTextInputFormatter.digitsOnly
                                ],
                                decoration: InputDecoration(
                                    hintText: 'Duration in Days',
                                    contentPadding:
                                    EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                                    enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.black),
                                    ),
                                    border: OutlineInputBorder(
                                        borderSide: BorderSide(color: Colors.black))),
                                onSaved: (value) {
                                  context.read<AdvertiseTripBloc>().trip.duration = int.parse(value!);
                                },),
                            ),

                            Padding(
                              padding: const EdgeInsets.all(10),
                              child: TextFormField(
                                onTap: (){
                                  _displayDateAndTimePicker();
                                },
                                controller: _datetimeController,
                                validator: (value) => value == null || value.length < 3 ? 'Can\'t be empty' : null,
                                decoration: InputDecoration(
                                    hintText: 'Date & Time',
                                    contentPadding:
                                    EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                                    enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.black),
                                    ),
                                    border: OutlineInputBorder(
                                        borderSide: BorderSide(color: Colors.black))),
                                onSaved: (value) {
                                  context.read<AdvertiseTripBloc>().trip.schedule = value;
                                },),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(10),
                              child: MaterialButton(
                                onPressed: () {
                                  if(widget.isForEditing) {
                                    _updateTrip();
                                  } else {
                                    _addTripToFirestore();
                                  }
                                },
                                minWidth: double.infinity,
                                height: 50,
                                color: Colors.blue,
                                elevation: 0,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(50),
                                ),
                                child: Text(
                                  widget.isForEditing ? 'Update Trip' : 'Add Trip',
                                  style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 18,
                                      color: Colors.white),
                                ),
                              ),
                            )
                          ],
                        ),

                      ],
                    ),
                  ),
                ),
              ),
            ),

            bloc.isLoading ? Container(
              height: MediaQuery.of(context).size.height,
              width:  MediaQuery.of(context).size.width,
              color: Colors.white.withOpacity(0.5),
              child: Center(child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    height: 40,
                    child: CircularProgressIndicator(),
                  )
                ],
              )),
            ) : SizedBox(),
          ],
        ),
      ),
    );
  }
}

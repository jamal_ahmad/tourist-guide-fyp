import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:provider/provider.dart';
import 'package:travel_hour/blocs/advertise_trip_bloc.dart';
import 'package:travel_hour/models/trip.dart';
import 'package:travel_hour/pages/trip_details.dart';
import 'package:travel_hour/utils/next_screen.dart';

import 'advertise_trip.dart';

class AdvertisedTripsList extends StatefulWidget {
  bool isFromUser = false;

  AdvertisedTripsList({Key? key, this.isFromUser = false}) : super(key: key);

  @override
  _AdvertisedTripsListState createState() => _AdvertisedTripsListState();
}

class _AdvertisedTripsListState extends State<AdvertisedTripsList> {
  @override
  initState() {
    _initialize();
    super.initState();
  }

  _initialize() async {
    SchedulerBinding.instance?.addPostFrameCallback((timeStamp) {
      final bloc = context.read<AdvertiseTripBloc>();
      if (widget.isFromUser) {
        bloc.getAllAdvertisedTrips(context);
      } else {
        bloc.getMyAdvertisedTrips(context);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final bloc = context.watch<AdvertiseTripBloc>();
    TextStyle _textStyle = TextStyle(
        fontSize: 16, fontWeight: FontWeight.w500, color: Colors.grey[900]);
    TextStyle _bodyTextStyle = TextStyle(
        fontSize: 14, fontWeight: FontWeight.w400, color: Colors.grey[600]);
    TextStyle _captionTextStyle = TextStyle(
        fontSize: 12, fontWeight: FontWeight.w400, color: Colors.grey[500]);
    final tripsList = bloc.trips;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        title: Text(
          widget.isFromUser ? 'Upcoming Trips' : 'Your Trips',
          style: TextStyle(fontSize: 20),
        ),
        centerTitle: true,
      ),
      body: SafeArea(
        child: SingleChildScrollView(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Row(),
            tripsList.isEmpty
                ? bloc.isLoading
                    ? Container(
                        height: 150,
                        child: Center(
                          child: SizedBox(
                            height: 30,
                            child: CircularProgressIndicator(),
                          ),
                        ),
                      )
                    : SizedBox()
                : ListView.builder(
                    itemCount: tripsList.length,
                    shrinkWrap: true,
                    primary: false,
                    itemBuilder: (context, i) {
                      final _trip = tripsList[i];

                      return InkWell(
                        onTap: (){
                          Navigator.of(context).push(MaterialPageRoute(builder: (context)=> TripDetails(trip: _trip,)));
                        },
                        child: Container(
                          height: 110,
                          margin:
                              EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              ClipRRect(
                                borderRadius: BorderRadius.circular(10),
                                child: Container(
                                  height: 100,
                                  width: 100,
                                  child: Hero(
                                    tag: 'upcoming${_trip.id}',
                                    child: CachedNetworkImage(
                                      imageUrl: _trip.imgUrl!,
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: 15,
                              ),
                              Expanded(
                                child: Row(
                                  children: [
                                    Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          _trip.name ?? ' ',
                                          style: _textStyle,
                                        ),
                                        Text(
                                          'From : ${_trip.from ?? ' '}',
                                          style: _bodyTextStyle,
                                        ),
                                        Text(
                                          "To : ${_trip.to ?? ' '}",
                                          style: _bodyTextStyle,
                                        ),
                                        Text(
                                          "ON : ${_trip.schedule ?? ' '}",
                                          style: _captionTextStyle,
                                        ),
                                        Text(
                                          "For : ${_trip.duration ?? ' '} days",
                                          style: _captionTextStyle,
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              Column(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Row(
                                    mainAxisSize: MainAxisSize.min,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      Icon(
                                        Icons.money,
                                        color: Colors.deepPurpleAccent,
                                        size: 12,
                                      ),
                                      Text(
                                        "${_trip.price ?? ' '}",
                                        style: _captionTextStyle.copyWith(
                                            color: Colors.black),
                                      ),
                                    ],
                                  ),
                                  widget.isFromUser
                                      ? SizedBox()
                                      : SizedBox(
                                    height: 25,
                                        child: IconButton(
                                        onPressed: () {
                                          nextScreen(context, AdvertiseTrip(isForEditing: true, trip: _trip));
                                        },
                                        icon: Icon(
                                          CupertinoIcons.pencil_circle,
                                          color: Colors.redAccent,
                                        )),
                                      ),
                                  widget.isFromUser
                                      ? SizedBox()
                                      : IconButton(
                                          onPressed: () {
                                            _displayDeleteDialog(_trip);
                                          },
                                          icon: Icon(
                                            Icons.delete,
                                            color: Colors.redAccent,
                                          )),
                                ],
                              )
                            ],
                          ),
                        ),
                      );
                    },
                  )
          ],
        )),
      ),
    );
  }

  _displayDeleteDialog(Trip _trip) {
    final bloc = context.read<AdvertiseTripBloc>();
    showDialog(
        context: context,
        builder: (_ctx) {
          TextStyle _textStyle = TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w500,
              color: Colors.grey[900]);
          return AlertDialog(
            content: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  'Are you sure?',
                  style: _textStyle.copyWith(
                      color: Colors.redAccent, fontWeight: FontWeight.w700),
                ),
                SizedBox(
                  height: 15,
                ),
                Text(
                  'Are you sure you want to delete this trip?',
                  textAlign: TextAlign.center,
                  style: _textStyle,
                ),
                SizedBox(
                  height: 15,
                ),
                Row(
                  children: [
                    Expanded(
                      child: SizedBox(
                        height: 32,
                        child: ElevatedButton(
                          onPressed: () {
                            bloc.deleteTrip(context, _trip);
                            Navigator.of(context).pop();
                          },
                          style: ElevatedButton.styleFrom(
                              primary: Colors.redAccent),
                          child: Text(
                            "YES",
                            style: _textStyle.copyWith(color: Colors.white),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Expanded(
                      child: SizedBox(
                        height: 32,
                        child: ElevatedButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          style: ElevatedButton.styleFrom(
                              primary: Colors.greenAccent),
                          child: Text(
                            "NO",
                            style: _textStyle.copyWith(color: Colors.white),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          );
        });
  }
}

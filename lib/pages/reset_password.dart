import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:travel_hour/blocs/company_registration_bloc.dart';

class PasswordResetWidget extends StatefulWidget {
  const PasswordResetWidget({Key? key}) : super(key: key);

  @override
  State<PasswordResetWidget> createState() => _PasswordResetWidgetState();
}

class _PasswordResetWidgetState extends State<PasswordResetWidget> {
  @override
  Widget build(BuildContext context) {
    final bloc = context.watch<CompanyRegistrationBloc>();
    return WillPopScope(
      onWillPop: () async {
        return !bloc.isLoading;
      },
      child: Scaffold(
        key: bloc.scaffoldKey,
        body: Stack(
          children: [

            SafeArea(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 10),
                child: Form(
                  key: bloc.formKey,
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(18.0),
                        child: Container(
                          child: Text(
                            'Reset Password',
                            style: TextStyle(fontSize: 20),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(10),
                        child: TextFormField(
                          validator: (value) => value == null || !value.contains("@") || !value.contains(".") ? 'Must be a valid email' : null,
                          decoration: InputDecoration(
                              hintText: 'Email Address',
                              contentPadding:
                              EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.black),
                              ),
                              border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.black))),
                          onSaved: (value) {
                            context.read<CompanyRegistrationBloc>().company.email = value;
                          },),
                      ),

                      Padding(
                        padding: const EdgeInsets.all(10),
                        child: MaterialButton(
                          onPressed: () {
                            context.read<CompanyRegistrationBloc>().resetPassword(context);
                          },
                          minWidth: double.infinity,
                          height: 50,
                          color: Colors.blue,
                          elevation: 0,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(50),
                          ),
                          child: Text(
                            'Reset Password',
                            style: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 18,
                                color: Colors.white),
                          ),
                        ),
                      ),

                    ],
                  ),
                ),
              ),
            ),

            bloc.isLoading ? Container(
              height: MediaQuery.of(context).size.height,
              width:  MediaQuery.of(context).size.width,
              color: Colors.white.withOpacity(0.5),
              child: Center(child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    height: 40,
                    child: CircularProgressIndicator(),
                  )
                ],
              )),
            ) : SizedBox(),
          ],
        ),
      ),
    );
  }
}

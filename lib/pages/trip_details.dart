import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:travel_hour/models/company.dart';
import 'package:travel_hour/models/trip.dart';

class TripDetails extends StatefulWidget {
  Trip? trip;
  TripDetails({Key? key, this.trip}) : super(key: key);

  @override
  _TripDetailsState createState() => _TripDetailsState();
}

class _TripDetailsState extends State<TripDetails> {
  Company? _company;
  bool isLoading = false;

  @override
  initState() {
    super.initState();
    if (widget.trip != null) {
      _getCompanyDetails(widget.trip!.companyId!);
    }
  }

  _getCompanyDetails(String companyId) async {
    isLoading = true;
    setState(() {});
    final FirebaseFirestore _fireStore = FirebaseFirestore.instance;
    _fireStore.collection('companies').doc(companyId).get().then((value) async {
      if (value != null && value.exists && value.data() != null) {
        final data = value.data()!;
        this._company = Company.fromJson(data);
        isLoading = false;
        setState(() {});
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final squareSize = size.height > size.width ? size.width : size.height;
    final _trip = widget.trip!;

    TextStyle _textStyle = TextStyle(
        fontSize: 18, fontWeight: FontWeight.w700, color: Colors.black);
    TextStyle _headStyle = TextStyle(
        fontSize: 16, fontWeight: FontWeight.w700, color: Colors.grey[900]);
    TextStyle _bodyTextStyle = TextStyle(
        fontSize: 14, fontWeight: FontWeight.w400, color: Colors.grey[600]);
    TextStyle _captionTextStyle = TextStyle(
        fontSize: 12, fontWeight: FontWeight.w400, color: Colors.grey[500]);

    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: squareSize,
              height: squareSize,
              child: Hero(
                  tag: 'upcoming${_trip.id}',
                  child: CachedNetworkImage(
                    imageUrl: _trip.imgUrl!,
                    fit: BoxFit.cover,
                  )),
            ),
            Padding(
              padding: EdgeInsets.all(15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    _trip.name ?? ' ',
                    style: _textStyle,
                  ),
                  Divider(),
                  SizedBox(height: 10),
                  _buildCompanyProfile(),
                  SizedBox(height: 10),
                  Divider(),
                  SizedBox(height: 10),
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.location_on,
                        color: Colors.deepPurpleAccent,
                        size: 14,
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Text(
                        'Trip Location',
                        style: _headStyle,
                      ),
                    ],
                  ),
                  SizedBox(height: 5),
                  Text(
                    'From : ${_trip.from ?? ' '}',
                    style: _bodyTextStyle,
                  ),
                  Text(
                    "To : ${_trip.to ?? ' '}",
                    style: _bodyTextStyle,
                  ),
                  Divider(),
                  SizedBox(height: 10),
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.access_time,
                        color: Colors.deepPurpleAccent,
                        size: 12,
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Text(
                        'Trip Schedule',
                        style: _headStyle,
                      ),
                    ],
                  ),
                  SizedBox(height: 5),
                  Text(
                    "${_trip.schedule ?? ' '}",
                    style: _captionTextStyle,
                  ),
                  Divider(),
                  SizedBox(height: 10),
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.bedtime,
                        color: Colors.deepPurpleAccent,
                        size: 12,
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Text(
                        'Stay Duration',
                        style: _headStyle,
                      ),
                    ],
                  ),
                  SizedBox(height: 5),
                  Text(
                    "For : ${_trip.duration ?? ' '} days",
                    style: _captionTextStyle,
                  ),
                  Divider(),
                  SizedBox(height: 10),
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.money,
                        color: Colors.deepPurpleAccent,
                        size: 12,
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Text(
                        'Charges (per head)',
                        style: _headStyle,
                      ),
                    ],
                  ),
                  SizedBox(height: 5),
                  Text(
                    "${_trip.price ?? ' '}",
                    style: _captionTextStyle.copyWith(color: Colors.black),
                  ),
                  SizedBox(height: 20),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildCompanyProfile() {
    TextStyle _textStyle = TextStyle(
        fontSize: 18, fontWeight: FontWeight.w700, color: Colors.black);
    TextStyle _bodyTextStyle = TextStyle(
        fontSize: 14, fontWeight: FontWeight.w400, color: Colors.grey[600]);
    return this.isLoading
        ? CircularProgressIndicator()
        : Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(100),
                child: Container(
                  height: 45,
                  width: 45,
                  child: CachedNetworkImage(
                    imageUrl: this._company!.imgUrl,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              SizedBox(
                width: 15,
              ),
              Row(
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        this._company?.name ?? ' ',
                        style: _textStyle,
                      ),
                      // SizedBox(height: 5,),
                      Text(
                        this._company?.phone ?? ' ',
                        style: _bodyTextStyle,
                      ),
                    ],
                  ),
                  // Spacer(),
                  // Create button with copy icon
                  IconButton(
                    icon: Icon(Icons.copy),
                    onPressed: () {
                      Clipboard.setData(
                              ClipboardData(text: this._company?.phone))
                          .then((_) {
                        Fluttertoast.showToast(
                            msg: "Phone number copied to clipboard",
                            toastLength: Toast.LENGTH_SHORT,
                            gravity: ToastGravity.BOTTOM,
                            timeInSecForIosWeb: 1,
                            backgroundColor: Colors.black,
                            textColor: Colors.white,
                            fontSize: 16.0);
                      });
                      // _launchCaller(this._company?.phone);
                    },
                  ),
                ],
              )
            ],
          );
  }
}

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:travel_hour/blocs/company_registration_bloc.dart';
import 'package:travel_hour/pages/reset_password.dart';

class CompanySignInWidget extends StatefulWidget {
  final String? tag;
  const CompanySignInWidget({Key? key, this.tag}) : super(key: key);

  @override
  _CompanySignInWidgetState createState() => _CompanySignInWidgetState();
}

class _CompanySignInWidgetState extends State<CompanySignInWidget> {




  @override
  Widget build(BuildContext context) {

    final bloc = context.watch<CompanyRegistrationBloc>();

    return WillPopScope(
      onWillPop: () async {
        return !bloc.isLoading;
      },
      child: Scaffold(
        key: bloc.scaffoldKey,
        body: Stack(
          children: [
            SafeArea(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 10),
                child: Form(
                  key: bloc.formKey,
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(18.0),
                        child: Container(
                          child: Text(
                            'Login Here',
                            style: TextStyle(fontSize: 20),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(10),
                        child: TextFormField(
                          validator: (value) => value == null || !value.contains("@") || !value.contains(".") ? 'Must be a valid email' : null,
                          decoration: InputDecoration(
                              hintText: 'Email Address',
                              contentPadding:
                              EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.black),
                              ),
                              border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.black))),
                          onSaved: (value) {
                            context.read<CompanyRegistrationBloc>().company.email = value;
                          },),
                      ),

                      Padding(
                        padding: const EdgeInsets.all(10),
                        child: TextFormField(
                          obscureText: true,
                          validator: (value) => value == null || value.length < 6 ? 'Must be 6 characters long' : null,
                          decoration: InputDecoration(
                              hintText: 'Password',
                              contentPadding:
                              EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.black),
                              ),
                              border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.black))),
                          onSaved: (value) {
                            context.read<CompanyRegistrationBloc>().company.password = value;
                          },),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(10),
                        child: MaterialButton(
                          onPressed: () {
                            context.read<CompanyRegistrationBloc>().loginCompany(widget.tag, context);
                          },
                          minWidth: double.infinity,
                          height: 50,
                          color: Colors.blue,
                          elevation: 0,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(50),
                          ),
                          child: Text(
                            'Login',
                            style: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 18,
                                color: Colors.white),
                          ),
                        ),
                      ),

                      Padding(
                          padding: const EdgeInsets.all(10),
                          child: TextButton(
                            onPressed: (){
                              Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) => ChangeNotifierProvider(
                                  create: (ctx) => CompanyRegistrationBloc(ctx),
                                  child: PasswordResetWidget(),
                                )),
                              );
                            },
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  'Forgot Password?',
                                  style: TextStyle(
                                      fontWeight: FontWeight.w400,
                                      fontSize: 14,
                                      color: Colors.black54),
                                ),
                              ],
                            ),
                          )
                      ),

                    ],
                  ),
                ),
              ),
            ),


            bloc.isLoading ? Container(
              height: MediaQuery.of(context).size.height,
              width:  MediaQuery.of(context).size.width,
              color: Colors.white.withOpacity(0.5),
              child: Center(child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    height: 40,
                    child: CircularProgressIndicator(),
                  )
                ],
              )),
            ) : SizedBox(),
          ],
        ),
      ),
    );
  }
}
